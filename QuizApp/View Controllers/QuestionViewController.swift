//
//  QuestionViewController.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    @IBOutlet weak var answerStack: UIStackView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    var questions: [Question]!
    var answers = [Answer]()
    var questionIndex = Int()
    var responses = [AnswerType]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    func updateUI() {
        answers = questions[questionIndex].answers.shuffled()
        emptyAnswerStackView()
        
        navigationItem.title = "Question \(questionIndex + 1)/\(questions.count)"
        
        switch questions[questionIndex].type {
        case .single:
            addSingleAnswerViews(answers)
        case .multiple:
            addMultipleAnswerViews(answers)
        case .ranged:
            addRangedAnswerViews(questions[questionIndex].answers)
        }
        
        questionLabel.text = questions[questionIndex].text
        updateProgressView(forQuestion: questionIndex + 1, ofTotal: questions.count)
    }
    
    func nextQuestion() {
        questionIndex += 1
        
        if questionIndex < questions.count {
            updateUI()
        } else {
            performSegue(withIdentifier: "ResultsSegue", sender: nil)
        }
    }
    
    func addSingleAnswerViews(_ answers: [Answer]) {
        for (index, answer) in answers.enumerated() {
            let button = UIButton()
            button.setTitleColor(self.view.tintColor, for: .normal)
            button.setTitle(answer.text, for: .normal)
            button.addTarget(self, action: #selector(singleAnswerButtonPressed), for: .touchUpInside)
            button.tag = index
            answerStack.addArrangedSubview(button)
        }
    }
    
    func addMultipleAnswerViews(_ answers: [Answer]) {
        for answer in answers {
            let stackView = UIStackView()
            stackView.alignment = .fill
            stackView.distribution = .fill
            stackView.spacing = 20
            let label = UILabel()
            label.text = answer.text
            let toggleSwitch = UISwitch()
            stackView.addArrangedSubview(label)
            stackView.addArrangedSubview(toggleSwitch)
            answerStack.addArrangedSubview(stackView)
        }
        
        let button = UIButton()
        button.setTitleColor(self.view.tintColor, for: .normal)
        button.setTitle("Submit Answer", for: .normal)
        button.addTarget(self, action: #selector(multipleAnswerButtonPressed), for: .touchUpInside)
        answerStack.addArrangedSubview(button)
        
    }
    
    func addRangedAnswerViews(_ answers: [Answer]) {
        let slider = UISlider()
        slider.setValue(0.5, animated: false)
        answerStack.addArrangedSubview(slider)
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        let label1 = UILabel()
        label1.text = answers.first?.text
        stackView.addArrangedSubview(label1)
        let label2 = UILabel()
        label2.text = answers.last?.text
        stackView.addArrangedSubview(label2)
        answerStack.addArrangedSubview(stackView)
        
        let button = UIButton()
        button.setTitleColor(self.view.tintColor, for: .normal)
        button.setTitle("Submit Answer", for: .normal)
        button.addTarget(self, action: #selector(rangedAnswerButtonPressed), for: .touchUpInside)
        answerStack.addArrangedSubview(button)
    }

    func updateProgressView(forQuestion question: Int, ofTotal total:Int) {
        let progress = Float(question) / Float(total)
        progressView.setProgress(progress, animated: true)
    }

    func emptyAnswerStackView() {
        for subView in answerStack.arrangedSubviews {
            answerStack.removeArrangedSubview(subView)
        }
    }
    
    @objc func singleAnswerButtonPressed(sender: UIButton) {
        let answer = questions[questionIndex].answers[sender.tag]
        responses += answer.type
        nextQuestion()
    }
    
    @objc func multipleAnswerButtonPressed() {
        for (index, subView) in answerStack.arrangedSubviews.enumerated() {
            guard let stackView = subView as? UIStackView else { continue }
            guard let toggleSwitch = stackView.arrangedSubviews[1] as? UISwitch else { continue }
            if toggleSwitch.isOn {
                responses += answers[index].type
            }
        }
        
        nextQuestion()
    }
    
    @objc func rangedAnswerButtonPressed() {
        guard let slider = answerStack.arrangedSubviews.first as? UISlider else { return }
        let index = Int(round(slider.value * Float(answers.count - 1)))
        responses += answers[index].type
        nextQuestion()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ResultsSegue" {
            let resultsViewController = segue.destination as! ResultsViewController
            resultsViewController.responses = responses
            resultsViewController.questionSet = questions.first!.set
        }
    }
    
}

