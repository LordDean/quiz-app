//
//  QuestionData.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

let eatingQuestions: [Question] = [
    Question(text: "What do you feel like most?",
                   type: .single,
                   set: .eating,
                   answers: [
                    Answer(text: "Pasta", type: [EatingAnswerType.italian]),
                    Answer(text: "Noodles", type: [EatingAnswerType.japanese]),
                    Answer(text: "Rice", type: [EatingAnswerType.japanese, EatingAnswerType.indian]),
                    Answer(text: "Bread", type: [EatingAnswerType.italian, EatingAnswerType.indian]),
                    Answer(text: "Chips", type: [EatingAnswerType.american]),
                    Answer(text: "I don't eat carbs...", type: [])
        ]),
    Question(text: "What is your favourite fruit/vegetable?",
                   type: .single,
                   set: .eating,
                   answers: [
                    Answer(text: "Tomato", type: [EatingAnswerType.italian]),
                    Answer(text: "Cabbage", type: [EatingAnswerType.japanese]),
                    Answer(text: "Potato", type: [EatingAnswerType.american]),
                    Answer(text: "Onion", type: [EatingAnswerType.indian]),
                    Answer(text: "Kale", type: [])
        ]),
    Question(text: "Which of the following can you eat?",
                   type: .multiple,
                   set: .eating,
                   answers: [
                    Answer(text: "Meat", type: [EatingAnswerType.italian, EatingAnswerType.american]),
                    Answer(text: "Gluten", type: [EatingAnswerType.italian, EatingAnswerType.american]),
                    Answer(text: "Fish", type: [EatingAnswerType.japanese]),
                    Answer(text: "Seaweed", type: [EatingAnswerType.japanese, EatingAnswerType.japanese]),
                    Answer(text: "Dairy", type: [EatingAnswerType.american, EatingAnswerType.italian, EatingAnswerType.indian]),
                    Answer(text: "Curry", type: [EatingAnswerType.indian, EatingAnswerType.indian])
        ]),
    Question(text: "Which of these cities have you been to?",
                   type: .multiple,
                   set: .eating,
                   answers: [
                    Answer(text: "Rome", type: [EatingAnswerType.italian]),
                    Answer(text: "Mumbai", type: [EatingAnswerType.indian]),
                    Answer(text: "Tokyo", type: [EatingAnswerType.japanese]),
                    Answer(text: "New York City", type: [EatingAnswerType.american])
        ]),
    Question(text: "How hungry are you?",
                   type: .ranged,
                   set: .eating,
                   answers: [
                    Answer(text: "Not at all", type: []),
                    Answer(text: "A bit", type: [EatingAnswerType.japanese]),
                    Answer(text: "Moderately", type: [EatingAnswerType.indian]),
                    Answer(text: "Fairly", type: [EatingAnswerType.american]),
                    Answer(text: "Extremely", type: [EatingAnswerType.italian])
        ]),
    Question(text: "How spicy do you like your food?",
                   type: .ranged,
                   set: .eating,
                   answers: [
                    Answer(text: "Not at all", type: [EatingAnswerType.american]),
                    Answer(text: "Mild", type: [EatingAnswerType.italian]),
                    Answer(text: "Hot", type: [EatingAnswerType.japanese]),
                    Answer(text: "Very Hot", type: [EatingAnswerType.indian])
        ]),
]

let dayOffQuestions: [Question] = [
    Question(text: "What is the weather like?",
             type: .single,
             set: .dayOff,
             answers: [
                Answer(text: "Hot", type: [DayOffAnswerType.aquarium]),
                Answer(text: "Sunny", type: [DayOffAnswerType.picnic]),
                Answer(text: "Mild", type: [DayOffAnswerType.zoo]),
                Answer(text: "Raining", type: [DayOffAnswerType.museum]),
        ]),
    Question(text: "What sort of activity would you like?",
             type: .single,
             set: .dayOff,
             answers: [
                Answer(text: "Indoor", type: [DayOffAnswerType.museum, DayOffAnswerType.aquarium]),
                Answer(text: "Outdoor", type: [DayOffAnswerType.zoo, DayOffAnswerType.picnic])
        ]),
    Question(text: "Which of these do you like?",
             type: .multiple,
             set: .dayOff,
             answers: [
                Answer(text: "Moving around", type: [DayOffAnswerType.zoo, DayOffAnswerType.museum]),
                Answer(text: "Relaxing", type: [DayOffAnswerType.picnic, DayOffAnswerType.aquarium]),
                Answer(text: "Animals", type: [DayOffAnswerType.zoo, DayOffAnswerType.aquarium]),
                Answer(text: "Reading", type: [DayOffAnswerType.picnic, DayOffAnswerType.museum])
        ]),
    Question(text: "What food do you like?",
             type: .multiple,
             set: .dayOff,
             answers: [
                Answer(text: "Sandwiches", type: [DayOffAnswerType.picnic, DayOffAnswerType.museum]),
                Answer(text: "Salad", type: [DayOffAnswerType.picnic, DayOffAnswerType.aquarium]),
                Answer(text: "Hot Chips", type: [DayOffAnswerType.aquarium, DayOffAnswerType.zoo]),
                Answer(text: "Fruit", type: [DayOffAnswerType.museum, DayOffAnswerType.zoo])
        ]),
    Question(text: "How much do you like animals?",
             type: .ranged,
             set: .dayOff,
             answers: [
                Answer(text: "Not at all", type: [DayOffAnswerType.picnic]),
                Answer(text: "A bit", type: [DayOffAnswerType.museum]),
                Answer(text: "Fairly", type: [DayOffAnswerType.aquarium]),
                Answer(text: "A lot", type: [DayOffAnswerType.zoo])
        ]),
    Question(text: "What is your budget?",
             type: .ranged,
             set: .dayOff,
             answers: [
                Answer(text: "$", type: [DayOffAnswerType.picnic]),
                Answer(text: "$$", type: [DayOffAnswerType.museum]),
                Answer(text: "$$$", type: [DayOffAnswerType.aquarium]),
                Answer(text: "$$$$", type: [DayOffAnswerType.zoo])
        ]),
]
