//
//  Answer.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct Answer {
    var text: String
    var type: [AnswerType]
}

protocol AnswerType {
    var name: String { get }
    var definition: String { get }
}
