//
//  ShuffleArray.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

extension Array {
    func shuffled() -> [Element] {
        var inputArray = self
        var shuffledArray = [Element]()
        
        for _ in self {
            let randomNumber = Int(arc4random_uniform(UInt32(inputArray.count)))
            let randomItem = inputArray.remove(at: randomNumber)
            shuffledArray.append(randomItem)
        }
        
        return shuffledArray
    }
}
