//
//  IntroViewController.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let navigationController = segue.destination
        let questionViewController = navigationController.children.first! as! QuestionViewController
        
        if segue.identifier == "EatingSegue" {
            questionViewController.questions = eatingQuestions.shuffled()
        } else if segue.identifier == "DayOffSegue" {
            questionViewController.questions = dayOffQuestions.shuffled()
        }
    }
    
    @IBAction func unwindToIntro(segue: UIStoryboardSegue) { }

}

