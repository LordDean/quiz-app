//
//  AnswerData.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

enum EatingAnswerType: AnswerType {
    case italian, japanese, indian, american, none
    
    var name: String {
        switch self {
        case .italian:
            return "Italian"
        case .japanese:
            return "Japanese"
        case .indian:
            return "Indian"
        case .american:
            return "American"
        case .none:
            return "Maybe nothing..."
        }
    }
    
    var definition: String {
        switch self {
        case .italian:
            return "You should have Italian food. Pasta, Pizza, Tomato sauces and Cheese."
        case .japanese:
            return "You should have Japanese food. Sushi, Tempura, Okonomiyaki and Teriyaki."
        case .indian:
            return "You should have Indian food. Curries, Naan, Dal and Tandoori."
        case .american:
            return "You should have American food. Burgers, Chips, Fried Chicken and Pie."
        case .none:
            return "You don't seem very keen on eating. You might actually be better off not having anything?"
        }
    }
}

enum DayOffAnswerType: AnswerType {
    case zoo, aquarium, picnic, museum
    
    var name: String {
        switch self {
        case .zoo:
            return "Zoo"
        case .aquarium:
            return "Aquarium"
        case .picnic:
            return "Picnic"
        case .museum:
            return "Museum"
        }
    }
    
    var definition: String {
        switch self {
        case .zoo:
            return "You should go to the Zoo. Walk around and see the animals."
        case .aquarium:
            return "You should go to the Aquarium. Submerge yourself in aquatic life."
        case .picnic:
            return "You should go on a Picnic. Don't forget the blanket."
        case .museum:
            return "You should go to the Museum. Learn some cool facts from history."
        }
    }
}

