//
//  ResultsViewController.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    @IBOutlet weak var resultQuestionLabel: UILabel!
    @IBOutlet weak var resultAnswerLabel: UILabel!
    @IBOutlet weak var resultDefinitionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var responses: [AnswerType]!
    var questionSet: QuestionSet!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        
        switch questionSet! {
        case .eating:
            resultQuestionLabel.text = "What should I eat?"
            calculateResult(ofEatingAnswerType: responses as! [EatingAnswerType])
        case .dayOff:
            resultQuestionLabel.text = "What should I do?"
            calculateResult(ofDayOffAnswerType: responses as! [DayOffAnswerType])
        }
    }
    
    func calculateResult(ofEatingAnswerType responses: [EatingAnswerType]) {
        var frequencyOfAnswers = [EatingAnswerType: Int]()
        for response in responses {
            frequencyOfAnswers[response] = (frequencyOfAnswers[response] ?? 0) + 1
        }
        
        let mostCommonAnswer = frequencyOfAnswers.sorted { $0.1 > $1.1 }.first!
        if mostCommonAnswer.value > 2 {
            resultAnswerLabel.text = mostCommonAnswer.key.name
            resultDefinitionLabel.text = mostCommonAnswer.key.definition
            imageView.image = UIImage(named: mostCommonAnswer.key.name)
        } else {
            resultAnswerLabel.text = EatingAnswerType.none.name
            resultDefinitionLabel.text = EatingAnswerType.none.definition
            imageView.image = UIImage(named: "None")
        }
    }
    
    func calculateResult(ofDayOffAnswerType responses: [DayOffAnswerType]) {
        var frequencyOfAnswers = [DayOffAnswerType: Int]()
        for response in responses {
            frequencyOfAnswers[response] = (frequencyOfAnswers[response] ?? 0) + 1
        }
        
        let mostCommonAnswer = frequencyOfAnswers.sorted { $0.1 > $1.1 }.first!
        resultAnswerLabel.text = mostCommonAnswer.key.name
        resultDefinitionLabel.text = mostCommonAnswer.key.definition
        imageView.image = UIImage(named: mostCommonAnswer.key.name)
    }
    
    
}

