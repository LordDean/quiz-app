//
//  Question.swift
//  QuizApp
//
//  Created by Dean Mollica on 7/5/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct Question {
    var text: String
    var type: ResponseType
    var set: QuestionSet
    var answers: [Answer]
}

enum ResponseType {
    case single, multiple, ranged
}

enum QuestionSet {
    case eating, dayOff
}
